import React, { Component } from 'react';
import jsonData from './jsonData'

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDone: false
    }
  }
  handleClick = (event) => {
    const is = !this.state.isDone;
    this.setState({isDone: is});
  }
  render() {
    let {name} = this.props.item;
    return(
      <li className="myList__Item">
          <span className={this.state.isDone ? 'done' : ''}>{name}</span>
          <button onClick={this.handleClick}>done</button>
      </li>
    )
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: jsonData
    };
    // this.handleChange = this.handleChange.bind(this)
  }
  handleChange = (event) => {
    let searchQuery = event.target.value.toLowerCase();
    let renderConcats = jsonData.filter(item => {
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchQuery) !==-1;
    });
    this.setState({
        contacts: renderConcats
    })
  };
  render() {
    let { count, contacts } = this.state;
    return (
      <div className="App">
        <span>count: {count}</span>
          <input onChange={this.handleChange} type="text" />
          <ul>
          {
              contacts.map((item, i) => {
                return(
                    <ListItem key={i} item={item} />
                );
              })
          }
          </ul>
      </div>
    );
  }
}

export default App;
